class Adapter:
    def __init__(self):
        self.users = []
        self.posts = []
        self.tags = []

    def match(self, url): raise NotImplementedError

    def post_processing(self):
        pass
