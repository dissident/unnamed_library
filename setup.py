import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="unnamed_library",
    version="0.0.1",
    author="Stanislav Chereshkevich",
    author_email="chereshkevichsv@gmail.com",
    description="Library for mapping diffent types of feeds",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dissident/unnamed_library",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
