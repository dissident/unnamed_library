import feedparser
from unnamed_library.adapters.adapter import Adapter


class RssAdapter(Adapter):
    def parse(self, url):
        feed_entries = feedparser.parse(url).entries
        for entry in feed_entries:
            self.__add_if_not_added(self.users, self.__authors(entry))
            self.__add_if_not_added(self.tags, self.__tags(entry))
            self.__add_if_not_added(self.posts, self.__post(entry))
        return self

    def __authors(self, entry):
        return map(lambda author: {'name': author.name}, entry.authors)

    def __tags(self, entry):
        return map(lambda tag: {'name': tag.term}, entry.tags)

    def __post(self, entry):
        return [{
            'title': entry.title,
            'body': entry.summary,
            'published': entry.get('published') or None
        }]

    def __add_if_not_added(self, attr, list):
        for item in list:
                if item not in attr:
                    attr.append(item)
