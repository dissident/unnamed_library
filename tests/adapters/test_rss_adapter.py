import vcr
from unnamed_library.adapters.rss_adapter import RssAdapter


rss_vcr = vcr.VCR(
    serializer='yaml',
    cassette_library_dir='fixtures/cassettes',
)

REDIT_RSS_URL = "https://www.reddit.com/r/news/.rss"
HABR_RSS_URL = "https://habr.com/rss/hubs/all"


@rss_vcr.use_cassette()
def test_redit_rss_adapter_users():
    reddit_feed = RssAdapter().parse(REDIT_RSS_URL)
    assert len(reddit_feed.users) == 23


@rss_vcr.use_cassette()
def test_redit_rss_adapter_tags():
    reddit_feed = RssAdapter().parse(REDIT_RSS_URL)
    assert len(reddit_feed.tags) == 1


@rss_vcr.use_cassette()
def test_redit_rss_adapter_posts():
    reddit_feed = RssAdapter().parse(REDIT_RSS_URL)
    assert len(reddit_feed.posts) == 25


@rss_vcr.use_cassette()
def test_habr_rss_adapter_users():
    harb_feed = RssAdapter().parse(HABR_RSS_URL)
    assert len(harb_feed.users) == 20


@rss_vcr.use_cassette()
def test_habr_rss_adapter_tags():
    harb_feed = RssAdapter().parse(HABR_RSS_URL)
    assert len(harb_feed.tags) == 160


@rss_vcr.use_cassette()
def test_habr_rss_adapter_posts():
    harb_feed = RssAdapter().parse(HABR_RSS_URL)
    assert len(harb_feed.posts) == 20
