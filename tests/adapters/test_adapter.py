from unnamed_library.adapters.adapter import Adapter


def test_adapter_init_state():
    adapter = Adapter()
    assert adapter.users == []
    assert adapter.tags == []
    assert adapter.posts == []
